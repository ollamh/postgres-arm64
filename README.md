# Postgres 14-bullseye for ARM64v8 image

## Build instructions

```bash
$ export DOCKER_CLI_EXPERIMENTAL=enabled
$ docker run --rm --privileged docker/binfmt:66f9012c56a8316f9244ffd7622d7c21c1f6f28d
$ docker buildx create --use --name multi-arch-builder
$ docker buildx build --builder multi-arch-builder --platform linux/arm64/v8 -t registry.gitlab.com/ollamh/postgres-arm64:14-bullseye --load .
$ docker push registry.gitlab.com/ollamh/postgres-arm64:14-bullseye
```

